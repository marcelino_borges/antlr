grammar Field;

field: name=Name NL (points NL)+ (burial NL)+ EOF;

points: treasure=Name WS 'marca' WS value=Double WS 'pontos';
burial: treasure=Name WS 'está' WS 'enterrado' WS 'em' WS at=location;
location: x=Int ',' y=Int;

Name: '"' ('A'..'Z' | 'a'..'z' | ' ' )+ '"' ;
Int: ('0'..'9')+;
Double: Int ('.'Int)?;

WS: (' ' | '\t')+;
NL:  '\r'? '\n';


/*


grammar Field;

field:
    nome=Nome NL
    (pontos NL)+
    (enterrado NL)+
    EOF;

pontos: tesouro=Nome WS 'marca' WS valor=Int WS 'pontos';
enterrado: tesouro=Nome WS 'está' WS 'enterrado' WS 'em' WS em=localizacao;
localizacao: x=Int ',' y=Int;


Nome: '"' ('A'..'Z' | 'a'..'z' | ' ')+ '"' ;
Int: ('0'..'9')+;

WS: (' ' | '\t')+;
NL:  '\r'? '\n';

 */