import java.util.Map;
import java.util.Scanner;

/**
 * @author alex.collins
 */
public class Game {
    private String name;
    private Map<String,Double> points;
    private String[][] grid;
    private int score = 0;
    public Double totalScore = 0d;

    public Game(String name, Map<String, Double> points, String[][] grid, Double totalScore) {
        this.name = name;
        this.points = points;
        this.grid = grid;
        this.totalScore = totalScore;
    }

    public void play() {
        Scanner scan = new Scanner(System.in);
        String sair = "sair";

        System.out.println("Você está jogando " + name + ". Encontra um tipo de cada tesouro e colete " + totalScore + " pontos para ganhar.") ;     

        
        while (true) {
            System.out.println("Onde deseja cavar (digite x ou \"sair\" para encerrar, depois y?");
             
            String input = scan.next();  

            if(input.equals(sair)) {
                System.out.println("Jogo encerrado. Até mais!");
                System.exit(0);
                scan.close();
                break;
            } else {
                int x = Integer.parseInt(input);
                int y = scan.nextInt();                

                if (grid[x][y] != null) {
                    String treasure = grid[x][y];
                    score += points.get(treasure);
                    grid[x][y] = null;
                    System.out.println("Você encontrou " + treasure + "! Sua pontuação " + score + ".");

                    if(score >= totalScore) {
                        winGame();
                    }
                } else {
                    System.out.println("Desculpe, nada aqui!");
                }
            }
        }        
    }

    void winGame() {
        System.out.println("Você ganhou!");
        System.exit(0);
    }
}
